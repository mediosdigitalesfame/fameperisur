<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Refacciones</title>
    <?php include('contenido/head.php'); ?>

</head>
<body>

<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
    <?php include('contenido/header.php'); ?>
    <?php include('contenido/analytics.php'); ?> 
    
	
		<!-- content 
			================================================== -->
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">         

				<div class="container">
					<h2>Refacciones y Accesorios</h2>
				</div>
			</div>

			<div class="about-box">
				<div class="container">
                
                

						<div class="col-md-12" align="center">
							<h3>Cotiza tu refacción</h3>
    
						</div>
                
					<div class="row">
				</div>
			</div>



<!--barra información limpia-->
		<div class="section">
			<div id="about-section">
				<div class="about-box">
				 <div class="container">
				 	 <div class="col-md-3" align="center">
					 </div>     	 
                     <div class="col-md-6" align="center">
							 <?php include('form.php'); ?>
					 </div>     	 
			     </div>
			 </div>



				<div class="welcome-box">
					<div class="container">
                
						<h1><span>Refacciones Originales Toyota</span></h1><br>
                        <h2>Confianza</h2>
						<p align="justify">Como parte de las ventajas de realizar los servicios de mantenimiento en los Distribuidores Toyota, está el uso e instalación de refacciones originales para tu automóvil, las cuales han sido fabricadas bajo los más altos estándares de calidad, durabilidad y rendimiento.

El uso de refacciones originales Toyota puede prolongar la vida de tu automóvil, ya que fueron diseñadas para ajustarse a las especificaciones exactas de tu Toyota.</p><br>

<p align="left"><strong>Horarios de atención</strong></p><br>   
<p align="justify">
- Lunes a Viernes.  8:30 a 19:00 hrs<br>
- Sábados.  9:00 a 14:00 hrs<br><br></p><br>


                	</div>
				</div>
 
               </div>
             </div>
	</div>
	<!-- End Container -->
        </div>
    </div>    
		 <?php include('contenido/footer.php'); ?>	 

</body>
</html>