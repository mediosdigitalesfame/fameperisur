<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Garantía Extendida</title>
    <?php include('contenido/head.php'); ?>
</head>
<body>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
        <?php include('contenido/header.php'); ?>
        <?php include('contenido/analytics.php'); ?>

		<!-- content 
			================================================== -->
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">         

				<div class="container">
					<h2>Garantía </h2>
				</div>
			</div>

			<div class="about-box">
				<div class="container">
					<div class="row">
				</div>
			</div>



<!--barra información limpia-->
		<div class="section">
			<div id="about-section">

				<div class="welcome-box">
					<div class="container">
						<h1><span>Confianza</span></h1><br>
						<p align="justify">Toyota Motor Sales de México, a través de la Red de Distribuidores Toyota en el país, te otorga la garantía de vehículos nuevos por <strong>36 meses o 60,000 kilómetros,</strong> lo que ocurra primero. Misma que cubre las reparaciones y los ajustes necesarios para corregir defectos en los materiales o en la mano de obra de cualquier parte suministrada por Toyota y está sujeta a las excepciones mencionadas en la Póliza de Garantía que entregamos con cada vehículo Toyota nuevo. En dicha póliza están incluidos todos los términos de cobertura y las recomendaciones para que tu garantía se conserve.<br>
<br>
Te recomendamos leer tu Póliza detenidamente, con el fin de conocer los alcances y ventajas que te ofrece, así como para revisar que todos los datos incluidos sean correctos y correspondan con tu automóvil.<br>
<br>
Una premisa indispensable para conservar tu garantía es realizar los servicios de mantenimiento a tu Toyota cada<strong> 6 meses o cada 10,000 kilómetros,</strong> lo que ocurra primero, en cualquiera de nuestros Distribuidores Autorizados.</p><br><br><br>
                 
                 
<h1><span>Exclusiones</span></h1><br>
						<p align="justify">La garantía Toyota no cubre de manera enunciativa, pero no limitativa los siguientes aspectos:<br><br>                 
<p align="justify">
- Reparaciones o mantenimiento inapropiados, incluyendo el uso de fluidos diferentes a los especificados en el <strong>Manual del Propietario.</strong><br>
- Modificaciones o instalación de accesorios o partes que no sean originales de Toyota.<br>
- Alteraciones en los medidores del vehículo, como es el odómetro.<br>
- Cualquier Alteración/Modificación del Vehículo.</p><br>
</p><br>

<p align="center">Te recomendamos leer tu <a href="pdfs/poliza.pdf" target="_blank"><strong>Póliza de Garantía,</strong></a> para mayor referencia..</p><br><br>


<h1><span>Garantía Extendida</span></h1><br>
<h2>Cobertura</h2><br>

						<p align="justify">Es un programa que te ofrece tu Distribuidor Toyota, el cual ampara a tu vehículo a partir de los <strong>3 años o 60,000 km</strong> que cubre la garantía otorgada por Toyota Motor Sales de México, S. de R.L. de C.V.

Al contratar el programa de Protección Extendida, tu vehículo quedará protegido hasta por <strong>6 años o 125,000 km,</strong> lo que ocurra primero. Tú podrás elegir uno de los tres planes disponibles de acuerdo a tus necesidades. Pregunta a tu Distribuidor Toyota cuáles son las condiciones y restricciones de la Protección Extendida.</p><br><br> 


<!--						<h1><span>Precios</span></h1><br>
							<div class="single-project-content">
								<img alt="" src="images/precio-garantia.png">
                         	</div>-->
                	</div>
				</div>          
                
		</div></div></div></div>
        <?php include('contenido/footer.php'); ?>

</body>
</html>