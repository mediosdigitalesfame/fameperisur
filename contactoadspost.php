<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Contacto</title>
	<?php include('contenido/head.php'); ?>
</head>

<body><em></em>

	<?php include('chat.php'); ?>

	<div id="container">
		<?php include('contenido/header.php'); ?>
		<?php include('contenido/analytics.php'); ?>

		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>Contáctanos</h2>
					

				</div>
			</div>

			<!-- contact box -->
			<div class="contact-box">
				<div class="container">
					<div class="row">

						<div class="col-md-6" align="center">

							 
						<div class="container">
							<div class="col-md-12" >
								<?php include('jotformpost.php'); ?>
							</div>
						</div>
					</div>

					<div class="col-md-3">
						<div class="contact-information">
							<h3>Información de Contacto</h3>
							<ul class="contact-information-list">
								<li><span><i class="fa fa-home"></i><span>Anillo Perif. 4000 Bis,</span> <span>Col. Jardines del Pedregal </span> <span> Del. Álvaro Obregón, México D.F.</span><span> C.P. 01900</span> </li>
								<li><span><i class="fa fa-phone"></i>(55) 5481 1900</span><br>
									<span><i class="fa fa-phone"></i>01 800 670 8386</span>
								</li>
								<li><i class="fa fa-phone"></i><span>Recepción <strong>Ext. 100 </strong></span><br>                                    
									<i class="fa fa-phone"></i><span>Taller de Servicio <strong>Ext. 224</strong></span><br>
									<i class="fa fa-phone"></i><span>Refacciones <strong>Ext. 145 </strong></span><br>
									<i class="fa fa-phone"></i><span>Postventa <strong>Ext. 136 </strong></span><br>
									<i class="fa fa-phone"></i><span>Ventas <strong>Ext. 110 </strong></span><br>      
									<i class="fa fa-phone"></i><span>Financiamiento <strong>Ext. 124 </strong></span><br>  </li>                                   


									<p>
										<h3>Whatsapp</h3>
										<li>
											<span><i class="fa fa-whatsapp"></i>
												<strong>  Ventas   y  Postventa <br>
													<a href="https://api.whatsapp.com/send?phone=524433250949&text=Hola,%20Quiero%20más%20información!" target="_blank" title="Ventas">
														4433250949 
													</a>
													| 
													<a href="https://api.whatsapp.com/send?phone=525532325380 &text=Hola,%20Quiero%20más%20información!" target="_blank" title="Postventa">
														5532325380 
													</a>
												</strong>
											</span>
										</li>
										<li>
											<span><i class="fa fa-whatsapp"></i>
												<strong>  Citas   y  Financiamiento <br>
													<a href="https://api.whatsapp.com/send?phone=525529221489 &text=Hola,%20Quiero%20más%20información!" target="_blank" title="Citas">
														5529221489 
													</a> | 
													<a href="https://api.whatsapp.com/send?phone=525539937307 &text=Hola,%20Quiero%20más%20información!" target="_blank" title="Financiamiento">
														5539937307 
													</a>
												</strong>
											</span>
										</li>

									</p>
									<li><a href="#"><i class="fa fa-envelope"></i>recepcion@fameperisur.com</a></li>


								</ul>
							</div>
						</div>

						<div class="col-md-3">
							<div class="contact-information">
								<h3>Horario de Atención</h3>
								<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en <strong>Toyota FAME Perisur</strong>; te escuchamos y atendemos de manera personalizada. </p>
								<h4>Sala de ventas</h4>
								<p class="work-time"><span>Lunes - Viernes</span>: 9:00 a.m. - 7:00 p.m.</p>
								<p class="work-time"><span>Sábado</span>: 9:00 a.m. - 6:00 p.m.</p>
								<p class="work-time"><span>Domingo</span>: 11:00 a.m. - 6:00 p.m.</p>                             
							</div>
						</div>


					</div>
				</div><br>
			</div>

			<?php include('contenido/footer.php'); ?>
		</div> 			
		
	</body>
	</html>