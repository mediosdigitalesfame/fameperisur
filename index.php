<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Toyota® FAME Perisur</title>
    <?php include('contenido/head.php'); ?>

 
</head>

<body>

<?php include('chat.php'); ?>


	<!-- Container -->
	<div id="container">
        <?php include('contenido/header.php'); ?>
        <?php include('contenido/analytics.php'); ?>
        <?php include('contenido/bienvenida.php'); ?>

        <!--  INICIO BANNERS -->
	    <div id="slider">
		    <div class="flexslider">
			    <ul class="slides">

                   

                  <!--  ____________________________________________________________________________________________________  -->   

                     <li>
                    	<a href="http://www.fameseminuevos.com/" target="_blank"><img alt="Autos Seminuevos" src="banners/seminuevos mexico.jpg" /></a>
					 </li>

				    <li>
                        <a href="http://notifame.com/" target="_self"><img alt="NotiFame" src="banners/notifame.jpg" /></a>
				    </li>

			    </ul>
		    </div>       

		    <img class="shadow-image" alt="" src="images/shadow.png">

            <?php include('contenido/vehiculos.php'); ?>
            <?php include('contenido/citas.php'); ?>

 	    </div>
    </div>

 	<?php include('contenido/footer.php'); ?>

</body>
</html>