<!-- Header -->
<header class="clearfix">
	<!-- Static navbar -->
	<div class="navbar navbar-default navbar-fixed-top">
		<div class="top-line">
			<div class="container">
				<p>
					<span><i class="fa fa-phone"></i>Agencia: (55) 5481 1900</span>
				</p>
				<ul class="social-icons">
					<li><a class="facebook" href="https://www.facebook.com/fameperisur/" target="_blank"><i class="fa fa-facebook"></i></a></li>
					<li><a class="twitter" href="https://twitter.com/ToyotaFame" target="_blank"><i class="fa fa-twitter"></i></a></li>
					<li><a class="youtube" href="https://www.youtube.com/user/GrupoFameAutos" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
					 
					<li>
						<a class="whatsapp" href="https://api.whatsapp.com/send?phone=524433250949&text=Hola,%20Quiero%20más%20información!" target="_blank">
							<i class="fa fa-whatsapp"></i>
						</a>
					</li>
					<li><a class="instagram" href="https://www.instagram.com/grupofame/" target="_self"><i class="fa fa-instagram"></i></a></li>
				</ul>
			</div>
		</div>

		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.php" target="_self"><img alt="Inicio" src="images/logo.png"></a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">

					<li><a class="active" href="index.php">Inicio</a></li>
					<li class="drop"><a href="autos.php">Autos</a>
						<ul class="drop-down">
							<li><a href="pdfs/avanza2019.pdf" target="_blank">Avanza</a></li>
									<li><a href="pdfs/camry2019.pdf" target="_blank">Camry</a></li>
									<li><a href="pdfs/camryh2019.pdf" target="_blank">Camry HV</a></li>
									<li><a href="pdfs/corolla2019.pdf" target="_blank">Corolla</a></li>
									<li><a href="pdfs/hiace2019.pdf" target="_blank">Hiace</a></li>
                                    <li><a href="pdfs/highlander2019.pdf" target="_blank">Highlander</a></li>
                                    <li><a href="pdfs/hilux2019.pdf" target="_blank">Hilux</a></li>
                                    <li><a href="pdfs/hiluxdiesel2019.pdf" target="_blank">Hilux Diesel</a></li>
                                    <li><a href="pdfs/landcruiser2019.pdf" target="_blank">Land Cruiser</a></li>
									<li><a href="pdfs/prius.pdf" target="_blank">Prius</a></li>
                                    <li><a href="pdfs/rav4.pdf" target="_blank">Rav 4</a></li>
                                    <li><a href="pdfs/sequoia.pdf" target="_blank">Sequoia</a></li>
									<li><a href="pdfs/sienna2019.pdf" target="_blank">Sienna</a></li>
									<li><a href="pdfs/tacoma2019.pdf" target="_blank">Tacoma</a></li>
									<li><a href="pdfs/tundra2019.pdf" target="_blank">Tundra</a></li>
									<li><a href="pdfs/yaris-hb.pdf" target="_blank">Yaris Hatchback</a></li>
									<li><a href="pdfs/yarissedan2019.pdf" target="_blank">Yaris Sedán</a></li>
									<li><a href="pdfs/yaris-r.pdf" target="_blank">Yaris R</a></li>    
						</ul> </li>                              
						<li class="drop"><a href="servicio.php">Servicio</a>
							<ul class="drop-down">
								<li><a href="servicio.php">Cita de Servicio</a></li>                     
								<li><a href="garantia.php">Garantía Extendida</a></li>
								<li><a href="refacciones.php">Refacciones</a></li>

								<li><a href="contacto.php" target="_self">Cotiza tu Auto</a></li> 
								<li><a href="manejo.php" target="_self">Prueba de Manejo</a></li>                            	

							</ul> </li>   

							                                                        

							<li class="drop"><a href="promociones.php">Promociones</a>
								<ul class="drop-down">
									<li><a href="financiamiento.php" target="_self">Financiamiento</a></li>
								</ul>   </li>                         
								<li><a href="http://fameseminuevos.com/" target="_blank">Seminuevos</a></li>
								
								<li><a href="ubicacion.php" target="_self">Ubicación</a></li>

								<li><a href="contacto.php" target="_self">Contacto</a></li>



							</ul>


						</div>
					</div>
				</div>

			


			</header>
		<!-- End Header -->