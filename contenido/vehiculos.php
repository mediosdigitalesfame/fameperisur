<!-- content  -->
	 <div id="content">
         <!-- VersaTAG -->
		 <!-- Latest Post -->
		 <div class="latest-post">
			 <div class="title-section">
				 <h1>Nuestros <span> Vehículos</span></h1>
				 <p>Navega a través de la sección para seleccionar el auto de tu preferencia</p>
			 </div>
                
             <!-- Inserta esta etiqueta donde quieras que aparezca Botón +1. 
             <div class="g-plusone" data-size="small" data-annotation="none"></div>  -->   
                
			 <div id="owl-demo" class="owl-carousel owl-theme">

			 	 <div class="item news-item">
	                 <center>
						 <a href="pdfs/chr2019.pdf" target="_blank"><img alt="CH-R 2019" src="images/autos/chr2019.png"></a>
						 <h2> <strong> <em> <font color="#000000"> CH-R  </font> <sup>&reg;</sup> <font color="#DD1707"> 2019</font> </em> </strong></h2>   
						 <a class="read-more" href="pdfs/chr2019.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                  </center>
 				 </div>

 				 <div class="item news-item">
	                 <center>
						 <a href="pdfs/hiluxdiesel4x42019.pdf" target="_blank"><img alt="Hilux diesel 4x4 2019" src="images/autos/hiluxdiesel4x42019.png"></a>
						 <h2> <strong> <em> <font color="#000000"> HILUX DIESEL 4X4 </font> <sup>&reg;</sup> <font color="#DD1707"> 2019</font> </em> </strong></h2>  
						 <a class="read-more" href="pdfs/hiluxdiesel4x42019.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                  </center>
 				 </div>

 				 <div class="item news-item">
	                 <center>
						 <a href="pdfs/yaris-r.pdf" target="_blank"><img alt="Yaris R 2018" src="images/autos/yarisr2018.jpg"></a>
						 <h2> <strong> <em> <font color="#000000"> YARIS R </font> <sup>&reg;</sup> <font color="#DD1707"> 2019</font> </em> </strong></h2>   
						 <a class="read-more" href="pdfs/yaris-r.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                  </center>
 				 </div>

                    
                 <div class="item news-item"><center>
					 <a href="pdfs/corolla2019.pdf" target="_blank"><img alt="Corolla 2018" src="images/autos/corolla2019.png"></a>
					 <h2> <strong> <em> <font color="#000000"> COROLLA </font> <sup>&reg;</sup> <font color="#DD1707"> 2019</font> </em> </strong></h2> 
					 <a class="read-more" href="pdfs/corolla2019.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
					 </center>
				 </div>
                    
                 <div class="item news-item"><center>
					 <a href="pdfs/prius.pdf" target="_blank"><img alt="Prius 2018" src="images/autos/prius2018.jpg"></a>
					 <h2> <strong> <em> <font color="#000000"> PRIUS </font> <sup>&reg;</sup> <font color="#DD1707"> 2019</font> </em> </strong></h2> 
					 <a class="read-more" href="pdfs/prius.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
					 </center>
				 </div>
   
                 <div class="item news-item"><center>
					 <a href="pdfs/prius-c2019.pdf" target="_blank"><img alt="Prius C 2019" src="images/autos/priusc2019.png"></a>
					 <h2> <strong> <em> <font color="#000000"> PRIUS-C </font> <sup>&reg;</sup> <font color="#DD1707"> 2019</font> </em> </strong></h2> 
					 <a class="read-more" href="pdfs/prius-c2019.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
					 </center>
				 </div>
				     
                 <div class="item news-item"><center>
					 <a href="pdfs/rav4.pdf" target="_blank">	<img alt="Rav4 2018" src="images/autos/rav42018.png"></a>
					 <h2> <strong> <em> <font color="#000000"> RAV 4 </font> <sup>&reg;</sup> <font color="#DD1707"> 2019</font> </em> </strong></h2> 
					 <a class="read-more" href="pdfs/rav4.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
					 </center>
				 </div>
                    
                 <div class="item news-item"><center>
					 <a href="pdfs/camry2019.pdf" target="_blank">	<img alt="Camry 2018" src="images/autos/camry2019.jpg"></a>
					 <h2> <strong> <em> <font color="#000000"> CAMRY </font> <sup>&reg;</sup> <font color="#DD1707"> 2019</font> </em> </strong></h2> 
					 <a class="read-more" href="pdfs/camry2019.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
					 </center>
				 </div>

				  <div class="item news-item"><center>
					 <a href="pdfs/camryh2019.pdf" target="_blank">	<img alt="Camry 2018" src="images/autos/camryh2019.png"></a>
					 <h2> <strong> <em> <font color="#000000"> CAMRY HV</font> <sup>&reg;</sup> <font color="#DD1707"> 2019</font> </em> </strong></h2> 
					 <a class="read-more" href="pdfs/camryh2019.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
					 </center>
				 </div>
                     
                 <div class="item news-item"><center>
					 <a href="pdfs/hilux2019.pdf" target="_blank">	<img alt="Hilux 2019" src="images/autos/hilux2019.png"></a>
					 <h2> <strong> <em> <font color="#000000"> HILUX </font> <sup>&reg;</sup> <font color="#DD1707"> 2019</font> </em> </strong></h2> 
					 <a class="read-more" href="pdfs/hilux2019.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
					 </center>
				 </div>          
          
				 <div class="item news-item"><center>
					 <a href="pdfs/avanza2019.pdf" target="_blank"><img alt="Avanza 2019" src="images/autos/avanza2019.png"></a>
					 <h2> <strong> <em> <font color="#000000"> AVANZA </font> <sup>&reg;</sup> <font color="#DD1707"> 2019</font> </em> </strong></h2> 
					 <a class="read-more" href="pdfs/avanza2019.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
					 </center>
				 </div>
                    
                 <div class="item news-item"><center>
					 <a href="pdfs/yarissedan2019.pdf" target="_blank"><img alt="Yaris 2018" src="images/autos/yarissedan2019.jpg"></a>
					 <h2> <strong> <em> <font color="#000000"> YARIS </font> <sup>&reg;</sup> <font color="#DD1707"> 2019</font> </em> </strong></h2> 
					 <a class="read-more" href="pdfs/yarissedan2019.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
					 </center>
				 </div>
                    
				 <div class="item news-item"><center>
					 <a href="pdfs/hiace2019.pdf" target="_blank">	<img alt="Hiace" src="images/autos/hiace2019.png"></a>
					 <h2> <strong> <em> <font color="#000000"> HIACE </font> <sup>&reg;</sup> <font color="#DD1707"> 2019</font> </em> </strong></h2> 
					 <a class="read-more" href="pdfs/hiace2019.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
					 </center>
				 </div>   
                    
				 <div class="item news-item"><center>
					 <a href="pdfs/highlander2019.pdf" target="_blank"><img alt="Highlander 2018" src="images/autos/highlander2019.jpg"></a>
					 <h2> <strong> <em> <font color="#000000"> HIGHLANDER </font> <sup>&reg;</sup> <font color="#DD1707"> 2019</font> </em> </strong></h2> 
					 <a class="read-more" href="pdfs/highlander2019.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
					 </center>
				 </div> 
                    
				 <div class="item news-item"><center>
					 <a href="pdfs/landcruiser2019.pdf" target="_blank"><img alt="Land Cruiser" src="images/autos/landcrusier2019.png"></a>
					 <h2> <strong> <em> <font color="#000000"> LAND CRUISER </font> <sup>&reg;</sup> <font color="#DD1707"> 2019</font> </em> </strong></h2> 
					 <a class="read-more" href="pdfs/landcruiser2019.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
					 </center>
				 </div>        
                    
				 <div class="item news-item"><center>
					 <a href="pdfs/sequoia.pdf" target="_blank"><img alt="Sequoia 2018" src="images/autos/sequoia2018.jpg"></a>
					 <h2> <strong> <em> <font color="#000000"> SEQUOIA </font> <sup>&reg;</sup> <font color="#DD1707"> 2019</font> </em> </strong></h2> 
					 <a class="read-more" href="pdfs/sequoia.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
					 </center>
				 </div> 

				 <div class="item news-item"><center>
					 <a href="pdfs/sienna2019.pdf" target="_blank"><img alt="Sienna 2018" src="images/autos/sienna2019.png"></a>
					 <h2> <strong> <em> <font color="#000000"> SIENNA </font> <sup>&reg;</sup> <font color="#DD1707"> 2019</font> </em> </strong></h2> 
					 <a class="read-more" href="pdfs/sienna2019.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
					 </center>
				 </div> 
                    
                 <div class="item news-item"><center>
					 <a href="pdfs/tacoma2019.pdf" target="_blank"><img alt="Tacoma 2018" src="images/autos/tacoma2019.png"></a>
					 <h2> <strong> <em> <font color="#000000"> TACOMA </font> <sup>&reg;</sup> <font color="#DD1707"> 2019</font> </em> </strong></h2> 
					 <a class="read-more" href="pdfs/tacoma2019.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
					 </center>
				 </div> 

				 <div class="item news-item"><center>
					 <a href="pdfs/tundra2019.pdf" target="_blank"><img alt="tundra 2018" src="images/autos/tundra2019.png"></a>
					 <h2> <strong> <em> <font color="#000000"> TUNDRA </font> <sup>&reg;</sup> <font color="#DD1707"> 2019</font> </em> </strong></h2> 
					 <a class="read-more" href="pdfs/tundra2019.pdf" target="_blank">Características<i class="fa fa-arrow-right"></i></a>
					 </center>
				 </div> 

				 <div class="item news-item"><center>
					 <a href="pdfs/yaris-hb.pdf" target="_blank"><img alt="Yaris Hatchback 2018" src="images/autos/yarishb2018.jpg"></a>
					 <h2> <strong> <em> <font color="#000000"> YARIS HB </font> <sup>&reg;</sup> <font color="#DD1707"> 2019</font> </em> </strong></h2> 
					 <a class="read-more" href="pdfs/yaris-hb.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
					</center>
				 </div> 
			 </div>
		 </div>
